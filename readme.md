## Para correrlo
- npm install
- npm install -g nodemon
- nodemon


## Stack utilizado
- Nodejs
- Express
- Jade
- Gulp
- Stylus
- MongoDB
- Angular(todavía no implementado)

## Cuestiones aún no resueltas
- Filtro Estrellas
- Filtro nombres
- Alineación flecha en menu responsive
- Errores MongoDB

## Anotaciones
- Le agregué a los botones un efecto de aparición
-"localhost:3000/hoteles" devuelve el listado completo de hoteles
- Hay código comentado que eventualmente podría resultar útil más adelante, al igual que el archivo filter.js
- La iteración de hoteles tiene un tope de 10 para evitar que colapse la página(linea 2 del list-hoteles.pug) la forma correcta sería agregar un paginado, pero sería agregarle más complejidad al ejercicio
-Para el filtro de estrellas podría utilizarse el valor de hotel.stars dentro de la iteración "for hotel in hoteles"
- El style.styl compila el style.css que a su vez se transforma en el estilo.min.css. Tal vez sea conveniente crear una carpeta aparte para producción y dejar solo el estilo.min.css en la carpeta stylesheets
- La base de datos de MongoDB aún no está funcionando, pero está parte de la estructura.
- El precio figura exactamente igual que figura en los datos, tal vez sea necesario quitarle los centavos y agregarle un punto a las centenas, el tamaño fue modificado para que se pueda ver el precio entero.
- El tamaño de la imagen de la hotel varía según el ancho de la ventana, haciendo que no todas las imágenes conserven el mismo alto. Otra opción sería incluir la imagen con un background-image dentro de un div.

##Librerías
- Bootstrap
- Accordionjs
