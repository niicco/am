var express = require('express');
var router = express.Router();
var fs = require("fs");
var data=require('../data/data.json');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Almundo',
    hoteles:data,
    titMenuResp: "Filtrar",
    titMenu: "Filtros",
    titMenuOne: "Nombre de hotel",
    stars: "Estrellas",
    allStars: "Todas las estrellas",
    viewHotel: "Ver hotel",
    priceBy: "Precio por noche por",
    room: "habitación",
    coin: "ARS"
  });
});
router.get('/hoteles', function(req, res) {
  res.json(data);

});
router.post('/filter', function(req, res) {
  console.log("búsqueda:"+req.body.txtsearch);
  res.send(req.body.txtsearch);
});
router.post('/stars', function(req, res) {
  console.log("búsqueda:"+req.body.stars);
  res.send("Datos recibidos")
});


module.exports = router;
