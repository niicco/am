var express = require('express');
var hoteles = express.Router();
var data=require('../data/data.json');
var HotelCtrl = require('../controllers/hoteles');
//models

var Hotel= require('../models/hotel');
hoteles.route('/hoteles')
  .get(HotelCtrl.findAllHotels)

hoteles.route('/hoteles/:id')
  .get(HotelCtrl.findById)



app.use('/api', hoteles);

module.exports = router;
