var mongoose = require('mongoose');
var Hotel  = mongoose.model('Hotel');

//GET - Return all tvshows in the DB
exports.findAllHotels = function(req, res) {
	Hotel.find(function(err, hoteles) {
    if(err) res.send(500, err.message);
    console.log('GET /hoteles')
		res.status(200).jsonp(hoteles);
	});
};
//GET - Return a TVShow with specified ID


exports.findAll = function(req, res){
  Hotel.find({},function(err, results) {
    return res.send(results);
  });
};
exports.findById = function(req, res){
  var id = req.params.id;
  Hotel.findOne({'_id':id},function(err, result) {
    return res.send(result);
  });
};
exports.addHotel = function(req, res) {
  Hotel.create(req.body, function (err, result) {
    if (err) return console.log(err);
    return res.send(result);
  });
}
exports.updateHotel = function(req, res) {
  var id = req.params.id;
  var updates = req.body;

  Hotel.updateHotel({"_id":id}, req.body,
    function (err, numberAffected) {
      if (err) return console.log(err);
      console.log('Updated %d Hotels', numberAffected);
      res.send(202);
  });
}
exports.deleteHotel = function(req, res){
  var id = req.params.id;
  Hotel.remove({'_id':id},function(result) {
    return res.send(result);
  });
};
