var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concatCss = require('gulp-concat-css');
var stylus = require('gulp-stylus');

gulp.task('stylus', function () {
    return gulp.src('public/stylesheets/*.styl')
      .pipe(stylus())
});

gulp.task('minify', function () {
    gulp.src('public/stylesheets/style.css')
        .pipe(concatCss("estilo.css"))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/stylesheets'));
});
/*
gulp.task('watch', ['minify'], function() {
  gulp.watch('public/stylesheets/*.css', ['minify']);
});
*/
gulp.task('watch', function () {
    gulp.watch('./path/*.styl', ['stylus']);
});

gulp.task('default', ['stylus']);
